package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class Calculator {
    private List<String> stringList;
    private int currentIndex;

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null || statement.isEmpty() || hasForbiddenSymbols(statement)) {
            return null;
        }
        stringList = getListOfStrings(statement);
        Double maybeResult = additionSubtractionCalculate();
        if (maybeResult == null || maybeResult.isNaN() || maybeResult.isInfinite()) {
            return null;
        }
        BigDecimal result = BigDecimal.valueOf(maybeResult);
        return result.setScale(4, RoundingMode.HALF_EVEN).stripTrailingZeros().toPlainString();
    }

    private boolean hasForbiddenSymbols(String statement) {
        String containForbiddenRegEx = ".*[^-+/*\\.\\d()].*";
        return statement.matches(containForbiddenRegEx);
    }

    private boolean hasNextString() {
        return currentIndex < stringList.size();
    }

    private List<String> getListOfStrings(String statement) {
        String operator = "[-+/*()]";
        StringBuilder previousString = new StringBuilder();
        List<String> list = new ArrayList<>();
        for (int i = 0; i < statement.length(); i++) {
            String symbol = String.valueOf(statement.charAt(i));
            if (symbol.matches(operator)) {
                if (previousString.length() > 0) {
                    list.add(previousString.toString());
                    previousString.replace(0, previousString.length(), "");
                }
                list.add(symbol);
            }
            else {
                previousString.append(symbol);
                if (i == statement.length() - 1) {
                    list.add(previousString.toString());
                }
            }
        }
        return list;
    }

    private Double additionSubtractionCalculate() {
        Double firstOrderResult = multiplicationDivisionCalculate();

        while (hasNextString()) {
            String operator = stringList.get(currentIndex);

            if (!operator.matches("[-+]")) {
                break;
            }
            else {
                currentIndex++;
            }

            Double secondOrderResult = multiplicationDivisionCalculate();
            if (secondOrderResult == null) {
                return null;
            }
            if (operator.equals("+")) {
                firstOrderResult += secondOrderResult;
            }
            else {
                firstOrderResult -= secondOrderResult;
            }
        }
        return firstOrderResult;
    }

    private Double multiplicationDivisionCalculate() {
        Double firstOrderResult = bracketsContentCalculate();

        while (hasNextString()) {
            String operator = stringList.get(currentIndex);

            if (!operator.matches("[/*]")) {
                break;
            }
            else {
                currentIndex++;
            }

            Double secondOrderResult = bracketsContentCalculate();
            if (secondOrderResult == null) {
                return null;
            }
            if (operator.equals("*")) {
                firstOrderResult *= secondOrderResult;
            }
            else {
                firstOrderResult /= secondOrderResult;
            }
        }
        return firstOrderResult;
    }

    private Double bracketsContentCalculate() {
        String previousString = stringList.get(currentIndex);

        if (previousString.equals("(")) {
            currentIndex++;
            Double intermediateResult = additionSubtractionCalculate();
            if (hasNextString() && stringList.get(currentIndex).equals(")")) {
                currentIndex++;
                return intermediateResult;
            }
            else {
                return null;
            }
        }
        currentIndex++;
        String digit = "\\d+(\\.\\d+)?";
        if (!previousString.matches(digit)) {
            return null;
        }
        return Double.parseDouble(previousString);
    }
}
