package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        int inputListLength = inputNumbers.size();
        if (!isTriangularNumber(inputListLength) || inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }

        int[][] pyramid = createBlankPyramid(inputListLength);
        Collections.sort(inputNumbers);

        int listIndex = 0;
        for (int i = 0; i < pyramid.length; i++) {
            int shift = 0;
            for (int j = 0; j <= i; j++) {
                pyramid[i][pyramid[i].length / 2 + (j - i + shift)] = inputNumbers.get(listIndex);
                listIndex++;
                shift++;
            }
        }
        return pyramid;
    }

    private boolean isTriangularNumber(int length) {
        int maybeSquareNumber = 8 * length + 1;
        return Math.sqrt(maybeSquareNumber) % 1 == 0;
    }

    private int[][] createBlankPyramid(int length) {
        int numberOfArrays = (int) (Math.sqrt(8 * length + 1) - 1) / 2;
        int arraysLength = 2 * numberOfArrays - 1;
        return new int[numberOfArrays][arraysLength];
    }
}
