package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }

        int count = 0;
        int continuedIndex = 0;
        for (Object xObject : x) {
            for (int i = continuedIndex; i < y.size(); i++) {
                if (y.get(i).equals(xObject)) {
                    count++;
                    continuedIndex = i;
                    break;
                }
            }
        }
        return count == x.size();
    }
}
